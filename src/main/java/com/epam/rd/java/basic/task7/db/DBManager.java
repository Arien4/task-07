package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static Connection connection;
    private static DBManager instance;

    private String getConnectionUrl() {
        Properties properties = new Properties();
        try (InputStream stream = Files.newInputStream(Paths.get("app.properties"))) {
            properties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty("connection.url");
    }

    public static synchronized DBManager getInstance() {
        try {
            if (instance == null) {
                instance = new DBManager();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }

    private DBManager() throws Exception {

        connection = DriverManager.getConnection(getConnectionUrl());

    }

    public List<User> findAllUsers() throws DBException{
        ResultSet resultSet;
        List<User> users = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM users")) {
            resultSet = ps.executeQuery();
            while (resultSet.next()) {
                User newUser = new User(resultSet.getInt(1), resultSet.getString(2));
                users.add(newUser);
            }
        } catch (SQLException e) {
            throw new DBException("Error finding all users", e);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException{
        int inserted = 0;
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO users (login) VALUES (?)")) {
            ps.setString(1, user.getLogin());
            inserted += ps.executeUpdate();
            user.setId(getUser(user.getLogin()).getId());
        } catch (SQLException e) {
            throw new DBException("Error inserting user", e);
        }
        return inserted > 0;
    }

    public boolean deleteUsers(User... users) throws DBException{
        int deleted = 0;
        for (User user : users) {
            try (PreparedStatement ps = connection.prepareStatement("DELETE FROM users WHERE login = ?")) {
                ps.setString(1, user.getLogin());
                deleted += ps.executeUpdate();
            } catch (SQLException e) {
                throw new DBException("Error deleting users", e);
            }
        }
        return deleted > 0;
    }

    public User getUser(String login) throws DBException{
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM users WHERE users.login = ?")) {
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return new User(rs.getInt(1), rs.getString(2));
        } catch (SQLException e) {
            throw new DBException("Error finding user by login " + login, e);
        }
    }

    public Team getTeam(String name) throws DBException{
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM teams WHERE teams.name = ?")) {
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return new Team(rs.getInt(1), rs.getString(2));
        } catch (SQLException e) {
            throw new DBException("Error finding team by name " + name, e);
        }
    }

    public List<Team> findAllTeams() throws DBException{
        List<Team> teams = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM teams")) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Team newTeam = new Team(resultSet.getInt(1), resultSet.getString(2));
                teams.add(newTeam);
            }
        } catch (SQLException e) {
            throw new DBException("Error finding all teams", e);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        int inserted = 0;
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO teams (name) VALUES (?)")) {
            ps.setString(1, team.getName());
            inserted += ps.executeUpdate();
            team.setId(getTeam(team.getName()).getId());
        } catch (SQLException e) {
            throw new DBException("Error inserting team. Transaction failed", e);
        }
        return inserted > 0;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        int inserted = 0;
        String insertString = "INSERT INTO users_teams VALUES(?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(insertString)) {
            connection.setAutoCommit(false);
            for (Team team : teams) {
                ps.setInt(1, user.getId());
                ps.setInt(2, team.getId());
                inserted += ps.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            System.out.println("Error inserting teams for user: " + user);
            if (connection != null) {
                try {
                    System.err.print("Transaction is being rolled back");
                    connection.rollback();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            throw new DBException("Transaction failed", e);
        }
        return inserted > 0;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(
                "SELECT teams.id, teams.name FROM teams " +
                        "JOIN users_teams ON teams.id = users_teams.team_id " +
                        "WHERE users_teams.user_id = ?")) {
            ps.setInt(1, user.getId());
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Team newTeam = new Team(resultSet.getInt(1), resultSet.getString(2));
                teams.add(newTeam);
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving teams fo user " + user.getLogin(), e);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        int deleted = 0;
        String deleteString = "DELETE FROM teams WHERE teams.name = ?";
        try (PreparedStatement ps = connection.prepareStatement(deleteString)) {
            ps.setString(1, team.getName());
            deleted = ps.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error deleting team " + team.getName(), e);
        }
        return deleted > 0;
    }

    public boolean updateTeam(Team team) throws DBException {
        int updated = 0;
        String updateString = "UPDATE teams SET name = ? WHERE id = ?";
        try (PreparedStatement ps = connection.prepareStatement(updateString)) {
            ps.setString(1, team.getName());
            ps.setInt(2, team.getId());
            updated = ps.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error updating team " + team.getName(), e);
        }
        return updated > 0;
    }

    public static void main(String[] args) {
        DBManager dbm = DBManager.getInstance();
        User roma = new User("Roma");
        Team teamA = new Team("AA");
        Team teamB = new Team("BB");

        try {
            dbm.insertUser(roma);
            dbm.insertTeam(teamA);
            dbm.insertTeam(teamB);
            dbm.setTeamsForUser(roma, teamA, teamB);
            System.out.println(dbm.getUserTeams(roma));
            teamA.setName("ZZ");
            dbm.updateTeam(teamA);
            System.out.println(dbm.getUserTeams(roma));
        } catch (DBException e) {
            e.printStackTrace();
        }


    }

}
